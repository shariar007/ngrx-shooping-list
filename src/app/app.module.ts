import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { ShopingListComponent } from './store/components/shoping-list/shoping-list.component';
import { AddItemComponent } from './store/components/add-item/add-item.component';
import {StoreModule} from "@ngrx/store";
import {ShoppingReducer} from "./store/reducers/shopping-list.reducers";

@NgModule({
  declarations: [
    AppComponent,
    ShopingListComponent,
    AddItemComponent
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot({shoppingReducer: ShoppingReducer}),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
