import {Action} from "@ngrx/store";
import {ActionType} from "../shared/enum/actionType.enum";

export class ShoopingActions implements Action{
   type: any;
   payload: any
}

// @ts-ignore
export class addItems implements ShoopingActions{
  type = ActionType.add;
  constructor(public payload: any) {}
}

// @ts-ignore
export class deleteItems implements ShoopingActions{
  type = ActionType.delete;
  constructor(public payload: any) {}
}
