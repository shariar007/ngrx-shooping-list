import { Component, OnInit } from '@angular/core';
import {ShoopingItem} from "../../models/shooping-item";
import { v4 as uuid} from "uuid"
import {addItems} from "../../actions/shooping.actions";
import {Store} from "@ngrx/store";
@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss']
})
export class AddItemComponent implements OnInit {

  constructor(public store: Store<{shoopingItem: ShoopingItem[]}>) { }

  ngOnInit() {
  }

  addItemIntoList(value: string) {
    const shoppingObj = new ShoopingItem();
    shoppingObj.name = value;
    shoppingObj.id = uuid();
    console.log(shoppingObj);
    this.store.dispatch(new addItems(shoppingObj));
    // this.shopping.addItemOnList(shoppingObj);
  }
}
