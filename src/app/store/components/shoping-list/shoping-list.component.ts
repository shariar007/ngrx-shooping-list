import { Component, OnInit } from '@angular/core';
import {select, Store} from "@ngrx/store";
import {ShoopingItem} from "../../models/shooping-item";
import { observable } from 'rxjs';

@Component({
  selector: 'app-shoping-list',
  templateUrl: './shoping-list.component.html',
  styleUrls: ['./shoping-list.component.scss']
})
export class ShopingListComponent implements OnInit {

  // @ts-ignore
  shoppingList: observable<ShoopingItem[]>;
  constructor(public store: Store<{shoppingReducer: ShoopingItem[]}>) {
    store.pipe(select('shoppingReducer')).subscribe(data => {
      this.shoppingList = data;
      console.log(this.shoppingList);
    });
  }

  ngOnInit() {
  }

}
