import {ShoopingItem} from "../models/shooping-item";
import {ShoopingActions} from "../actions/shooping.actions";
import {ActionType} from "../shared/enum/actionType.enum";

export const initialState: ShoopingItem[] = [
  { id: 'ab123-cd456-efg789-12345', name: 'coka cola'},
  { id: 'ab123-cd456-efg789-12346', name: 'Fanta'},
  { id: 'ab123-cd456-efg789-12347', name: 'Mojo'},
];

export function ShoppingReducer(state = initialState, action: ShoopingActions) {
  switch (action.type) {
    case ActionType.add:
      return [...state, action.payload];
    case ActionType.delete:
      [...state.splice(action.payload,1)];
      return [...state];
    default: return state;
  }
}
